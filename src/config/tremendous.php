<?php

return

    /*
    |--------------------------------------------------------------------------
    | TDS API Settings
    |--------------------------------------------------------------------------
    |
    |
    */
    [
        'v1' =>[
            'host' => env('TREMENDOUS_V1_HOST', 'https://reward.testflight.tremendous.com/v1'),
        ],
        'host' => env('TREMENDOUS_HOST', 'https://testflight.tremendous.com/api/'),
        'token' => env('TREMENDOUS_API_TOKEN'),
        'funding_source_id' => env('TREMENDOUS_FUNDING_SOURCE_ID'),
        'campaign_id' => env('TREMENDOUS_CAMPAIGN_ID'),
    ];