<?php

namespace SayThanks\Tremendous;

use Illuminate\Support\ServiceProvider;

class TremendousServiceProvider extends ServiceProvider
{

    public function register()
    {
        $this->mergeConfigFrom(__DIR__ . '/config/tremendous.php', 'tremendous');
    }

    public function boot()
    {
        $this->publishes(
            [
                __DIR__ . '/config/tremendous.php' => config_path('tremendous.php'),
            ], 'config');
    }


}