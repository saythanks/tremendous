<?php

namespace SayThanks\Tremendous;

use Illuminate\Http\Client\PendingRequest;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;
use SayThanks\Tremendous\Exceptions\InvalidConfigException;

class Tremendous
{

    public string $host;
    public string $host_v1;
    public string $funding_source_id;
    public string $campaign_id;
    private string $accessToken;


    public function __construct()
    {
        $this->host = config('tremendous.host');
        $this->accessToken = config('tremendous.token');
        $this->funding_source_id = config('tremendous.funding_source_id');
        $this->campaign_id = config('tremendous.campaign_id');
        $this->host_v1 = config('tremendous.v1.host');

        $this->validateConfig();
    }

    public function getProducts()
    {
        $url = '/v2/products';
        return $this->get($url);
    }

    public function createOrder(
        int $value,
        string $recipient_name,
        string $recipient_email,
        array $products = [],
        string $external_id = null,
        string $delivery_method = 'LINK',
        string $currency_code = 'ZAR',
        $funding_source_id = null,
        $campaign_id = null
    )
    {
        $url = '/v2/orders';
        $data = [
            'external_id' => $external_id,
            'payment' =>
                [
                    'funding_source_id' => $funding_source_id ?? $this->funding_source_id,
                ],
            'reward' =>
                [
                    'value' =>
                        [
                            'denomination' => $value,
                            'currency_code' => $currency_code,
                        ],
                    'campaign_id' => $campaign_id ?? $this->campaign_id,
                    'products' => $products,
                    'recipient' => [
                        'name' => $recipient_name,
                        'email' => $recipient_email,
                    ],
                    'delivery' =>
                        [
                            'method' => $delivery_method,
                            'meta' => [],
                        ]
                ],
        ];
        return $this->post($url, $data);
    }

    public function redeemOrder(
        string $v1_order_id,
        string $recipient_name,
        string $recipient_email,
        string $country_code,
        string $product_id
    ) {
        $url = "/payouts/";
        $data = [
            'id' => $v1_order_id,
            'payout' =>
                [
                    'email' => $recipient_email,
                    'name' => $recipient_name,
                    'country_code' => $country_code,
                    'catalog_id' => $product_id,
                ],
        ];
        return $this->v1_post($url, $data);
    }

    /**
     * @throws InvalidConfigException
     */
    protected function validateConfig(): void
    {
        if(!$this->host)
        {
            throw new InvalidConfigException("Unable to find Tremendous Host in your config!");
        }
        if(!$this->host_v1)
        {
            throw new InvalidConfigException("Unable to find Tremendous V1 Host in your config!");
        }
        if(!$this->accessToken)
        {
            throw new InvalidConfigException("Unable to find Tremendous Access Token in your config!");
        }
        if(!$this->campaign_id)
        {
            throw new InvalidConfigException("Unable to find Tremendous Campaign ID in your config!");
        }
        if(!$this->funding_source_id)
        {
            throw new InvalidConfigException("Unable to find Tremendous Funding Source ID in your config!");
        }
    }


    public function tremendousApi() : PendingRequest
    {
        return Http::retry(3, 100)
            ->asJson()
            ->acceptJson()
            ->withHeaders([
                'Authorization' => 'Bearer ' . $this->accessToken
            ]);
    }

    protected function post($urlPart, $data)
    {
        Log::info('Tremendous-API: Sending post', ['url' => $urlPart, 'data' => $data]);
        $response = $this->tremendousApi()->post($this->host . $urlPart, $data);
        if($response->successful())
        {
            Log::debug('Tremendous-API: Got response from post', ['url' => $urlPart, 'data' => $data, 'response-json' => $response->json()]);
        }
        else {
            Log::warning('Tremendous-API: Got invalid response from post', ['status' => $response->status(), 'url' => $urlPart, 'data' => $data, 'response-json' => $response->json()]);
        }
        return $response;
    }

    protected function v1_post($urlPart, $data)
    {
        Log::info('Tremendous-API: Sending post', ['url' => $urlPart, 'data' => $data]);
        $response = $this->tremendousApi()->post($this->host_v1 . $urlPart, $data);
        if($response->successful())
        {
            Log::debug('Tremendous-API: Got response from post', ['url' => $urlPart, 'data' => $data, 'response-json' => $response->json()]);
        }
        else {
            Log::warning('Tremendous-API: Got invalid response from post', ['status' => $response->status(), 'url' => $urlPart, 'data' => $data, 'response-json' => $response->json()]);
        }

        return $response;
    }

    protected function get($urlPart, $queryArray = [])
    {
        Log::info('Tremendous-API: Sending post', ['url' => $urlPart, 'data' => $queryArray,]);
        $response = $this->tremendousApi()->get($this->host . $urlPart, $queryArray);
        if($response->successful())
        {
            Log::debug('Tremendous-API: Got response from get', ['url' => $urlPart, 'data' => $queryArray, 'response-json' => $response->json()]);
        }
        else{
            Log::warning('Tremendous-API: Got invalid response from get', ['status' => $response->status(), 'url' => $urlPart, 'data' => $queryArray, 'response-json' => $response->json()]);
        }

        return $response;
    }

}
