# Tremendous Integration For Laravel

Provides API support for Tremendous ([Tremendous](https://www.tremendous.com/docs))

## About

Provides support for authenticaition as well as:
* Order Creation


## Installation

Add the repository to your composer respository section:
```json
"repositories": [
   {
      "type": "vcs",
      "url": "git@bitbucket.org:saythanks/tremendous.git"
   }
],
```

Require the package via composer:
```sh
composer require saythanks/tremendous
```

## Configuration

Add your environment variables:
```
TREMENDOUS_HOST=
TREMENDOUS_API_TOKEN=
TREMENDOUS_FUNDING_SOURCE_ID=
TREMENDOUS_CAMPAIGN_ID=
```

The defaults are set in `config/tremendous.php`. Publish the config to copy the file to your own config:
```sh
php artisan vendor:publish --provider="SayThanks\Tremendous\TremendousServiceProvider"
```




## Usage
